package model

import (
	"database/sql"
	"io"
	"mime/multipart"
	"os"
	"petstore/config"
	"petstore/constant/pet"
)

type Pet struct {
	Id    int64  `json:"id"`
	Name  string `json:"name"`
	Age   int64  `json:"age"`
	Photo string `json:"photo"`
}

var db *sql.DB

func init() {
	db = config.GetDb()
}
func (p *Pet) Create() error {
	res, err := db.Exec(pet.QUERY_PET_INSERT, p.Name, p.Age)
	if err != nil {
		return err
	}
	insertId, err := res.LastInsertId()
	if err != nil {
		return err
	}
	p.Id = insertId
	return err
}
func (p *Pet) Update() error {
	oldPetData := Pet{Id: p.Id}
	if err := oldPetData.Get(); err != nil {
		return err
	} else {
		if p.Age == 0 {
			p.Age = oldPetData.Age
		}
		if len(p.Name) == 0 {
			p.Name = oldPetData.Name
		}
		p.Photo = oldPetData.Photo
		_, err := db.Exec(pet.QUERY_PET_UPDATE, p.Name, p.Age, p.Id)
		return err

	}

}
func (p *Pet) Delete() error {
	_, err := db.Exec(pet.QUERY_PET_DELETE, p.Id)
	return err
}
func (p *Pet) Get() error {
	row := db.QueryRow(pet.QUERY_PET_GET, p.Id)
	err := row.Scan(&p.Id, &p.Name, &p.Age, &p.Photo)
	return err
}
func (p *Pet) UploadImage(file multipart.File, info *multipart.FileHeader) error {
	defer file.Close()
	fname := info.Filename
	fpath := "assets/" + fname
	out, err := os.OpenFile(fpath,
		os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return err
	}
	defer out.Close()
	io.Copy(out, file)
	_, err = db.Exec(pet.QUERY_PET_UPDATE_PHOTO, fpath, p.Id)
	if err != nil {
		return err
	}
	err = p.Get()
	return err

}
