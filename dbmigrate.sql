
CREATE DATABASE IF NOT EXISTS petstore;
create table if not EXISTS petstore.pet
(
	id int auto_increment primary key,
	name varchar(255) null,
	age int null,
	photo varchar(255) null
)
;