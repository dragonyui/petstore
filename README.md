# README #

Please import all go library first before building
go building refer to package name so when you checkout the package in your go src need to be the same 
`go get ./...` should solve all the library needed

### Building ###
just execute
`go build petstore`

### Running ###
`go run petstore` 
will do the trick
but you can also build it first then run the executable
`./petstore`

because i'm using flag library you can add several parameter,
 for more information you can execute 
`./petstore --help`

### DB settings ###
you can change database settings in petstore.conf by default 
or you can specify your own configuration file by using paramter -c
you can also use environment if it is defined in your config

### POSTMAN COLLECTION ###
if you are using postman you can use these collection 
https://www.getpostman.com/collections/f89eb1c46f494bd53f5c
