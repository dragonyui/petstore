package main

import (
	"petstore/controllers"

	"github.com/kataras/iris"
)

func main() {
	app := iris.New()
	app.StaticWeb("assets", "assets")
	controllers.ProcessController(app)
	app.Run(iris.Addr(":9999"))
}
