package constant

type ApiResponse struct {
	Response interface{} `json:"response"`
	Message  string      `json:"message"`
}

const RESPONSE_SUCCESS = "200"
const RESPONSE_ERROR = "500"

func GetSuccessResponse(response interface{}) ApiResponse {
	return ApiResponse{Response: response, Message: "Success"}
}
func GetErrorResponse(message string) ApiResponse {
	return ApiResponse{Message: message}
}
