package pet

const QUERY_PET_INSERT = "insert into pet (name,age) VALUES (?,?);"
const QUERY_PET_GET = `select id,name,age,ifnull(photo,'') from pet where id=? ;`
const QUERY_PET_UPDATE = `update pet set name=?, age=? where id=?`
const QUERY_PET_DELETE = `delete from pet  where id=?`
const QUERY_PET_UPDATE_PHOTO = `update pet set photo=? where id=?`
