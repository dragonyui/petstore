package location

const QUERY_PROVINCE_GET_ALL = `
select id,country,value from es_master_province;
`
const QUERY_PROVINCE_GET = `
select id,country,value from es_master_province limit $2 OFFSET $1;
`
const QUERY_PROVINCE_GET_BY_PARENT = `
select id,country,value from es_master_province where country=$1;
`
