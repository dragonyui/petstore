package constant

import (
	"github.com/kataras/iris/context"
)

const ACTION_METHOD_TYPE_GET = "GET"
const ACTION_METHOD_TYPE_POST = "POST"
const ACTION_METHOD_TYPE_PUT = "PUT"
const ACTION_METHOD_TYPE_DELETE = "DELETE"
const ACTION_METHOD_TYPE_ANY = "ANY"

type Controller struct {
	Name        string    `json:"name"`
	BasePath    string    `json:"base_path"`
	Actions     []*Action `json:"actions"`
	PreRequest  func(ctx context.Context)
	PostRequest func(ctx context.Context)
}

type Action struct {
	Path    string `json:"path"`
	Method  string `json:"method"`
	Process func(ctx context.Context)
}

func (c *Controller) Get(path string, process func(ctx context.Context)) {
	c.Actions = append(c.Actions, &Action{Path: path, Method: ACTION_METHOD_TYPE_GET, Process: process})
}
func (c *Controller) Post(path string, process func(ctx context.Context)) {
	c.Actions = append(c.Actions, &Action{Path: path, Method: ACTION_METHOD_TYPE_POST, Process: process})
}
func (c *Controller) Put(path string, process func(ctx context.Context)) {
	c.Actions = append(c.Actions, &Action{Path: path, Method: ACTION_METHOD_TYPE_PUT, Process: process})
}
func (c *Controller) Delete(path string, process func(ctx context.Context)) {
	c.Actions = append(c.Actions, &Action{Path: path, Method: ACTION_METHOD_TYPE_DELETE, Process: process})
}
func (c *Controller) Any(path string, process func(ctx context.Context)) {
	c.Actions = append(c.Actions, &Action{Path: path, Method: ACTION_METHOD_TYPE_ANY, Process: process})
}
