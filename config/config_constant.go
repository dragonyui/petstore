package config

const INI_KEY_DB_USERNAME = "db.username"
const INI_KEY_DB_PASSWORD = "db.password"
const INI_KEY_DB_HOST = "db.host"
const INI_KEY_DB_PORT = "db.port"
const INI_KEY_DB_SCHEMA = "db.schema"
const INI_KEY_DB_TYPE = "db.dbtype"
const INI_KEY_DB_MIGRATE_SCRIPT = "db.migratescriptpath"
