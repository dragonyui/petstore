package config

import (
	"flag"
	"log"
	"os"

	"github.com/go-ini/ini"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
)

var confFileLoc string
var env string
var cfg ini.File
var verboses bool
var exporterPath string

func init() {
	flag.StringVar(&env, "e", "production", "Environment Parameters (production,staging,local,etc) ")
	flag.StringVar(&confFileLoc, "c", "petstore.conf", "Config File Path")

	flag.Parse()
	config, err := ini.Load(confFileLoc)
	if err != nil {
		log.Println("Can't open config file /n creating new Config files")
		f, err := os.Create(confFileLoc)
		f.WriteString("")
		f.Close()
		config, err := ini.Load(confFileLoc)
		if err != nil {
			panic(err)
		}
		cfg = *config
	} else {
		cfg = *config
	}

	fillDbConf()
}
func fillDbConf() {
	getValueFromConfig(&db.host, env, INI_KEY_DB_HOST)
	getValueFromConfig(&db.username, env, INI_KEY_DB_USERNAME)
	getValueFromConfig(&db.password, env, INI_KEY_DB_PASSWORD)
	getValueFromConfig(&db.port, env, INI_KEY_DB_PORT)
	getValueFromConfig(&db.schema, env, INI_KEY_DB_SCHEMA)
	getValueFromConfig(&db.dbType, env, INI_KEY_DB_TYPE)
	getValueFromConfig(&db.dbMigrateScript, env, INI_KEY_DB_MIGRATE_SCRIPT)
}

func getValueFromConfig(container *string, environment string, key string) {
	if cfg.Section(environment).HasKey(key) {
		*container = cfg.Section(environment).Key(key).String()
	} else {
		*container = cfg.Section("").Key(key).String()
	}
}
