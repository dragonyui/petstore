package config

import (
	"database/sql"
	"log"

	"fmt"

	"errors"

	"io/ioutil"
	"strings"

	_ "github.com/lib/pq"
)

func init() {

}

type DBConf struct {
	username, password, host, port, schema, dbType, dbMigrateScript string
	db                                                              *sql.DB
}

func (conf *DBConf) getDbConnectionString() (string, error) {
	var connectionString string
	if conf.dbType == "postgres" {
		connectionString = fmt.Sprintf("posrtgres://%v:%v@%v:@%v/%v?sslmode=disable", conf.username, conf.password, conf.host, conf.port, conf.schema)
	} else if conf.dbType == "mysql" {
		connectionString = fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?multiStatements=true", conf.username, conf.password, conf.host, conf.port, conf.schema)
	} else {
		return "", errors.New("unsupported db Type")
	}
	return connectionString, nil
}

func (conf *DBConf) loadDb() *sql.DB {
	if conf.db == nil {
		connectionString, err := conf.getDbConnectionString()
		if err != nil {
			log.Fatal(err)
		}
		log.Println("initialize connection with connection string " + connectionString)
		newDb, err := sql.Open(conf.dbType, connectionString)
		if err != nil {
			log.Fatal(err)
		}
		_, errtestquery := newDb.Query("select 1")
		if errtestquery != nil {
			if strings.Contains(errtestquery.Error(), "1049") {
				log.Println(errtestquery)
				log.Println("initialize root database connection")
				rootDbConnString := fmt.Sprintf("%v:%v@tcp(%v:%v)/?multiStatements=true", conf.username, conf.password, conf.host, conf.port)
				dbRoot, errRoot := sql.Open(conf.dbType, rootDbConnString)
				if errRoot != nil {
					log.Fatal(errRoot)
				}
				b, err := ioutil.ReadFile(conf.dbMigrateScript)
				if err != nil {
					fmt.Print(err)
				}
				str := string(b)
				log.Println("execute script /n" + str)
				_, errMigrate := dbRoot.Exec(str)
				if errMigrate != nil {
					log.Fatal(errMigrate)
				}

				newDb, err = sql.Open(conf.dbType, connectionString)
				if err != nil {
					log.Fatal(err)
				}

			} else {
				log.Fatal(errtestquery)
			}
		}

		//dbExist := false
		//if res.Next() {
		//	var dbname string
		//	errscan := res.Scan(&dbname)
		//	if errscan != nil {
		//		log.Fatal(errscan)
		//	}
		//	dbExist = true
		//}
		//if !dbExist {
		//	log.Fatal("db not exist")
		//}
		conf.db = newDb
	}
	return conf.db
}

var db DBConf

func GetDb() *sql.DB {
	return db.loadDb()
}
