package controllers

import (
	"petstore/constant"
	"petstore/model"

	"strconv"

	"github.com/kataras/iris/context"
)

func init() {
	c := CreateNewControllerInstance("pet", "/")
	c.Post("/pet", NewPet)
	c.Get("/pet/{id:int}", GetPet)
	c.Put("/pet/{id:int}", UpdatePet)
	c.Delete("/pet/{id:int}", DeletePet)
	c.Post("/pet/{id:int}/uploadImage", UploadImage)

}
func NewPet(ctx context.Context) {
	p := &model.Pet{}
	if err := ctx.ReadJSON(p); err != nil {
		ctx.JSON(constant.GetErrorResponse("invalid parameters"))
		ctx.StatusCode(400)
	}
	if err := p.Create(); err != nil {
		ctx.JSON(constant.GetErrorResponse(err.Error()))
		ctx.StatusCode(500)
	} else {
		if len(p.Photo) > 0 {
			p.Photo = (ctx.Request().Host) + "/" + p.Photo
		}
		ctx.JSON(constant.GetSuccessResponse(p))
		ctx.StatusCode(201)
	}
}
func GetPet(ctx context.Context) {
	idParam := ctx.Params().Get("id")
	id, err := strconv.ParseInt(idParam, 10, 64)
	if err != nil {
		ctx.JSON(constant.GetErrorResponse(err.Error()))
		ctx.StatusCode(400)
	}
	p := &model.Pet{Id: id}
	if err := p.Get(); err != nil {
		ctx.JSON(constant.GetErrorResponse(err.Error()))
		ctx.StatusCode(404)
	} else {

		if len(p.Photo) > 0 {
			p.Photo = (ctx.Request().Host) + "/" + p.Photo
		}
		ctx.JSON(constant.GetSuccessResponse(p))
		ctx.StatusCode(200)
	}

}
func UpdatePet(ctx context.Context) {
	idParam := ctx.Params().Get("id")
	id, err := strconv.ParseInt(idParam, 10, 64)
	if err != nil {
		ctx.JSON(constant.GetErrorResponse(err.Error()))
		ctx.StatusCode(400)
	}

	p := &model.Pet{}
	if err := ctx.ReadJSON(p); err != nil {
		ctx.JSON(constant.GetErrorResponse("invalid parameters"))
		ctx.StatusCode(400)
	}
	p.Id = id
	if err := p.Update(); err != nil {
		ctx.JSON(constant.GetErrorResponse(err.Error()))
		ctx.StatusCode(500)
	} else {

		if len(p.Photo) > 0 {
			p.Photo = (ctx.Request().Host) + "/" + p.Photo
		}
		ctx.JSON(constant.GetSuccessResponse(p))
		ctx.StatusCode(202)
	}

}

func DeletePet(ctx context.Context) {
	idParam := ctx.Params().Get("id")
	id, err := strconv.ParseInt(idParam, 10, 64)
	if err != nil {
		ctx.JSON(constant.GetErrorResponse(err.Error()))
		ctx.StatusCode(400)
	}
	p := &model.Pet{Id: id}
	if err := p.Delete(); err != nil {
		ctx.JSON(constant.GetErrorResponse(err.Error()))
		ctx.StatusCode(500)
	} else {
		if len(p.Photo) > 0 {
			p.Photo = (ctx.Request().Host) + "/" + p.Photo
		}
		ctx.JSON(constant.GetSuccessResponse(p))
		ctx.StatusCode(202)
	}
}
func UploadImage(ctx context.Context) {
	idParam := ctx.Params().Get("id")
	id, err := strconv.ParseInt(idParam, 10, 64)
	if err != nil {
		ctx.JSON(constant.GetErrorResponse(err.Error()))
		ctx.StatusCode(400)
	}
	file, info, err := ctx.FormFile("image")
	if err != nil {
		ctx.StatusCode(400)
		ctx.JSON(constant.GetErrorResponse(err.Error()))
	}
	p := model.Pet{Id: id}
	if err := p.UploadImage(file, info); err != nil {
		ctx.StatusCode(500)
		ctx.JSON(constant.GetErrorResponse(err.Error()))
	} else {
		if len(p.Photo) > 0 {
			p.Photo = (ctx.Request().Host) + "/" + p.Photo
		}
		ctx.JSON(constant.GetSuccessResponse(p))
		ctx.StatusCode(202)
	}

}
