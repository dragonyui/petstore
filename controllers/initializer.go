package controllers

import (
	"log"
	"petstore/constant"

	"github.com/kataras/iris"
	"github.com/kataras/iris/context"
)

var controllers []*constant.Controller

func ProcessController(app *iris.Application) {
	if controllers == nil {
		log.Println("there aren't any controllers initialized yet..")
	} else {
		for _, c := range controllers {
			log.Println("Registering controller " + c.Name + " at " + c.BasePath)
			for _, a := range c.Actions {
				log.Println("----> Action " + a.Method + ":" + a.Path)
				switch a.Method {
				case constant.ACTION_METHOD_TYPE_GET:
					app.Get(c.BasePath+a.Path, a.Process)
					break
				case constant.ACTION_METHOD_TYPE_POST:
					app.Post(c.BasePath+a.Path, a.Process)
					break
				case constant.ACTION_METHOD_TYPE_PUT:
					app.Put(c.BasePath+a.Path, a.Process)
					break
				case constant.ACTION_METHOD_TYPE_DELETE:
					app.Delete(c.BasePath+a.Path, a.Process)
					break
				default:
					app.Any(c.BasePath+a.Path, a.Process)
					break
				}
			}
		}
	}
}

func CreateNewControllerInstance(name string, basePath string) *constant.Controller {
	c := constant.Controller{
		Name:        name,
		BasePath:    basePath,
		PreRequest:  func(ctx context.Context) {},
		PostRequest: func(ctx context.Context) {},
		Actions:     make([]*constant.Action, 0),
	}
	addController(&c)
	return &c
}
func addController(controller *constant.Controller) {
	if controller == nil {
		controllers = make([]*constant.Controller, 0)
	}
	controllers = append(controllers, controller)
}
